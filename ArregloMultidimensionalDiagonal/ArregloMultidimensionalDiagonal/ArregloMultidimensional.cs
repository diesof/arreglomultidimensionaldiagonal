﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloMultidimensionalDiagonal
{
    class ArregloMultidimensional
    {
        private int[,] numeros = new int[4, 4];

        public void SolicitarNumeros()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    Console.Write($"Ingrese un número en la posición [{i},{j}]: ");
                    numeros[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        public void MostrarResultados()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (i == j)
                    {
                        Console.WriteLine($"Valor en la posición [{i},{j}] = {numeros[i, j]}");
                    }
                }
            }
        }
    }
}