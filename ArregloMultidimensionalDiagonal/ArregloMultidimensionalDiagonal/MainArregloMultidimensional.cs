﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloMultidimensionalDiagonal
{
    class MainArregloMultidimensional
    {
        static void Main(string[] args)
        {
            ArregloMultidimensional a = new ArregloMultidimensional();

            a.SolicitarNumeros();

            Console.WriteLine();

            a.MostrarResultados();

            Console.ReadKey();
        }
    }
}